package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    private LiveData<List<Book>> allBooks;
    private MutableLiveData<List<Book>> selectedBook = new MutableLiveData<>();

    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public void updateBook(Book updatedbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(updatedbook);
        });
    }

    public void insertBook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newbook);
        });
    }

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
           bookDao.deleteBook(id);
        });
    }

    public Book getBook(long id) {
        Book book = bookDao.getBook(id);
        return book;
    }

    public void deleteAllBooks() {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteAllBooks();
        });
    }

}
